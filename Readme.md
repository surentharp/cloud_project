# CLOUD MONITORING AND PERFORMANCE MEASUREMENT TOOLS

Here is a step-by-step guide on how to use the code to build and deploy a cloud-native monitoring application:

## *Containerize the application using Docker:*

Build a Docker image of the application locally using the command 
```
docker build -t my_cloud_project_repo .
```

Push the Docker image to an Amazon Elastic Container Registry (ECR) repository using the commands 
```
docker tag my_cloud_project_repo:latest 975049942997.dkr.ecr.us-east-1.amazonaws.com/my_cloud_project_repo:latest and docker push 975049942997.dkr.ecr.us-east-1.amazonaws.com/my_cloud_project_repo:latest
```

## *Deploy the application on a Kubernetes cluster:*

Create a Kubernetes cluster on Amazon Elastic Kubernetes Service (EKS) using the command 
```
aws eks create-cluster --region us-east-1 --name my-cloud-project-cluster --kubernetes-version 1.29 --role-arn arn:aws:iam::975049942997:role/MyCloudEKSRole --resources-vpc-config subnetIds=subnet-0cf0ca87e3d16bce4,subnet-04b5c282dc5be528e,subnet-0a7b5095283eda0dc,subnet-00f849295ed2f226d,securityGroupIds=sg-0a30e61e75a6228fb
```

Use the provided Python script that uses the Kubernetes client library to deploy the Flask application and expose it via a service.

## *Access the application:*

Access the service from a local machine for testing or direct use by port forwarding using the command 
```
kubectl port-forward svc/my-flask-service 5000:5000
```

Expose the service beyond the local server using ngrok with the command `ngrok http 5000`

You can host this code on GitHub and provide this documentation to guide users on how to use the code to build and deploy a cloud-native monitoring application.